#!/usr/bin/env bash

################################################################################
# Contains a set of functions used for creating higher-level scripted Git 
# functionality.
################################################################################

################################################################################
# Silently add an entry to the directory stack via pushd.
# Globals:
#   None
# Arguments:
#   directory (required)
# Returns:
#   The exit status of the pushd command.
# Usage: 
#   pushdir <directory>
################################################################################
pushdir() {
        if [[ ! -z $1 ]]; then
                local dir=$1
                if [[ -d $dir ]]; then
                        pushd "${dir}" > /dev/null 2>&1
                else
                        echo "No such directory: ${dir}"
                        exit 1
                fi
        else
                echo "No directory specified"
                exit 1
        fi
}

################################################################################
# Silently remove an entry from the directory stack via popd.
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   The exit status of the popd command.
# Usage: 
#   popdir
################################################################################
popdir() {
        popd > /dev/null 2>&1
}

################################################################################
# List the fully-qualified paths of all .git directories that exist in the 
# specified directory. If a directory is not specified, the current directory
# will be used.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to current directory
# Returns:
#   A list of paths.
# Usage: 
#   find_all_git_directories [directory]
################################################################################
find_all_git_directories() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        find "$( pwd )" -type d -name ".git"
        popdir
}

################################################################################
# List the fully-qualified paths of the top-level directory of all git worktrees
# that exist in the specified directory. If a directory is not specified, the
# current directory will be used.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to current directory
# Returns:
#   A list of paths.
# Usage: 
#   find_all_git_worktrees [directory]
################################################################################
find_all_git_worktrees() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        find "$( pwd )" -type d -name ".git" -printf "%h\n"
        popdir
}

################################################################################
# Return the name of the branch the specified git worktree is currently on. If a
# worktree directory is not specified, it is assumed the directory stack is 
# currently on a git worktree.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to current directory
# Returns:
#   A branch name
# Usage: 
#   get_current_branch [directory]
################################################################################
get_current_branch() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        git rev-parse --abbrev-ref HEAD
        popdir
}

################################################################################
# Determine whether or not the specified worktree is currently on the specified 
# branch. If a worktree directory is not specified, it is assumed the directory 
# stack is currently on a git worktree.
# Globals:
#   None
# Arguments:
#   branch (required)
#   directory (optional) - defaults to current directory
# Returns:
#   0 if the worktree is on the branch or 1 otherwise.
# Usage: 
#   is_on_branch <branch> [directory]
################################################################################
is_on_branch() {
        if [[ ! -z $1 ]]; then
                local branch=$1
                local dir="${2:-$( pwd )}"
                local current_branch
                current_branch="$( get_current_branch "${dir}" )"
                [[ $branch = "${current_branch}" ]]
        else
                echo "No branch specified"
                exit 1
        fi
}

################################################################################
# Determine whether or not the specified branch exists in the specified remote
# repository. If no remote repo name is specified, it will default to 'origin'.
# Globals:
#   None
# Arguments:
#   branch (required)
#   remote (optional) - defaults to 'origin'
# Returns:
#   0 if the branch is in the remote or 1 otherwise.
# Usage: 
#   is_branch_in_remote <branch> [remote]
################################################################################
is_branch_in_remote() {
        if [[ ! -z $1 ]]; then
                local branch=$1
                local remote="${2:-origin}"
                if git show-ref --verify --quiet "refs/remotes/${remote}/${branch}" > /dev/null 2>&1; then
                        return 0
                fi
        else
                echo "No branch specified"
                exit 1
        fi
        return 1
}

################################################################################
# Determine whether or not the specified branch exists locally in the specified
# worktree. If a worktree directory is not specified, it is assumed the 
# directory stack is currently on a git worktree.
# Globals:
#   None
# Arguments:
#   branch (required)
#   directory (optional) - defaults to current directory
# Returns:
#   0 if the branch exists locally in the worktree or 1 otherwise.
# Usage: 
#   is_branch_in_worktree <branch> [directory]
################################################################################
is_branch_in_worktree() {
        if [[ ! -z $1 ]]; then
                local branch=$1
                local dir="${2:-$( pwd )}"
                local cmdout=1
                pushdir "${dir}"
                        if git rev-parse --verify --quiet "${branch}" > /dev/null 2>&1; then
                                cmdout=0
                        fi
                popdir
        else
                echo "No branch specified"
                exit 1
        fi
        return $cmdout
}

################################################################################
# Create the specified branch in the specified worktree if it does not already 
# exist locally. If a worktree directory is not specified, it is assumed the 
# directory stack is currently on a git worktree.
# Globals:
#   None
# Arguments:
#   branch (required)
#   directory (optional) - defaults to current directory
# Returns:
#   0 if the branch was created or it already exists, or 1 otherwise.
# Usage: 
#   create_branch <branch> [directory]
################################################################################
create_branch() {
        if [[ -z $1 ]]; then
                local branch=$1
                local dir="${2:-$( pwd )}"
                local cmdout=0
                if ! is_branch_in_worktree "${branch}" "${dir}"; then
                    pushdir
                    if git branch "${branch}" >/dev/null 2>&1; then
                        cmdout=0
                    fi
                    popdir
                else
                    cmdout=0
                fi
        else
                echo "No branch specified"
                exit 1
        fi
        return $cmdout
}

################################################################################
# Delete the specified branch from the specified remote if it exists there. If 
# no remote is specified, it will default to 'origin'. This function assumes the
# directory stack is currently on a git worktree.
# Globals:
#   None
# Arguments:
#   branch (required)
#   remote (optional) - defaults to 'origin'
# Returns:
#    The exit status of the git command or 0 if the branch does not exist.
# Usage:
#    delete_branch_from_remote <branch> [remote]
################################################################################
delete_branch_from_remote() {
        if [[ ! -z $1 ]]; then
                local branch=$1
                local remote="${2:-origin}"
                if is_branch_in_remote "${branch}" "${remote}"; then
                        git push "${remote}" --delete "${branch}" > /dev/null 2>&1
                else
                        return 0
                fi
        else
                echo "No branch specified"
                exit 1
        fi
}

################################################################################
# Delete the specified branch from the specified worktree if it exists locally
# and is either fully merged with its upstream branch or is in HEAD if no 
# upstream was set. If a worktree directory is not specified, it is assumed the 
# directory stack is currently on a git worktree.
# Globals:
#   None
# Arguments:
#   branch (required)
#   directory (optional) - defaults to the current directory
# Returns:
#    The exit status of the git command or 0 if the branch does not exist.
# Usage:
#    delete_branch_from_worktree <branch> [directory]
################################################################################
delete_branch_from_worktree() {
        if [[ ! -z $1 ]]; then
                local branch=$1
                local dir="${2:-$( pwd )}"
                if is_branch_in_worktree "${branch}" "${dir}"; then
                        pushdir "${dir}"
                        git branch -d "${branch}" > /dev/null 2>&1
                        local cmdout=$?
                        popdir
                        return $cmdout
                else
                        return 0
                fi
        else
                echo "No branch specified"
                exit 1
        fi
}

################################################################################
# Determine whether or not the specified worktree contains untracked files. If a
# worktree directory is not specified, it is assumed the directory stack is 
# currently on a git worktree.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to the current directory
# Returns:
#    0 if untracked files are present or 1 otherwise
# Usage:
#    has_untracked_files [directory]
################################################################################
has_untracked_files() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        local untracked_files
        untracked_files="$( git ls-files --others --exclude-standard )"
        popdir
        [[ ! -z $untracked_files ]]
}

################################################################################
# Determine whether or not the specified worktree contains unstaged changes. If 
# a worktree directory is not specified, it is assumed the directory stack is 
# currently on a git worktree.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to the current directory
# Returns:
#    0 if unstaged changes are present or 1 otherwise
# Usage:
#    has_unstaged_changes [directory]
################################################################################
has_unstaged_changes() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        local unstaged_changes
        unstaged_changes="$( git diff --name-only --ignore-submodules )"
        popdir
        [[ ! -z $unstaged_changes ]]
}

################################################################################
# Determine whether or not the specified worktree contains staged, but 
# uncommitted changes. If a worktree directory is not specified, it is assumed 
# the directory stack is currently on a git worktree.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to the current directory
# Returns:
#    0 if staged, but uncommitted changes are present or 1 otherwise
# Usage:
#    has_staged_changes [directory]
################################################################################
has_staged_changes() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        local staged_changes
        staged_changes="$( git diff --cached --name-only --ignore-submodules )"
        popdir
        [[ ! -z $staged_changes ]]
}

################################################################################
# Determine whether or not the specified worktree contains local commits that 
# have not been pushed to a specified remote. If no remote is specified, it will
# default to 'origin'.This function assumes the directory stack is currently on 
# a git worktree.
# Globals:
#   None
# Arguments:
#   remote (optional) - defaults to 'origin'
# Returns:
#    0 if unpushed commits are present or 1 otherwise
# Usage:
#    has_unpushed_commits [remote]
################################################################################
has_unpushed_commits() {
        local remote="${1:-origin}"
        local current_branch
        current_branch="$( get_current_branch )"
        if is_branch_in_remote "${branch}" "${remote}"; then
                local local_head_commit
                local_head_commit="$( git rev-parse --verify "${branch}" )"
                local remote_head_commit
                remote_head_commit="$( git rev-parse --verify "${remote}/${branch}" )"
                [[ $local_head_commit = "${remote_head_commit}" ]]
        else
                return 1
        fi
}

################################################################################
# Check out the specified branch in the specified worktree. If a worktree 
# directory is not specified, it is assumed the directory stack is currently on
# a git worktree.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to current directory
# Returns:
#    the exit status of the git command
# Usage:
#    check_out_branch <branch> [directory]
################################################################################
check_out_branch() {
        if [[ ! -z $1 ]]; then
                local branch=$1
                local dir="${2:-$( pwd)}"
                pushdir "${dir}"
                git checkout "${branch}" > /dev/null 2>&1
                local cmdout=$?
                popdir
                return $cmdout
        else
                echo "No branch specified"
                exit 1
        fi
}

################################################################################
# Pull updates for the current branch in the specified worktree. If a worktree 
# directory is not specified, it is assumed the directory stack is currently on
# a git worktree.
# Globals:
#   None
# Arguments:
#   directory (optional) - defaults to current directory
# Returns:
#    the exit status of the git command
# Usage:
#    check_out_branch <branch> [directory]
################################################################################
update_current_branch() {
        local dir="${1:-$( pwd )}"
        pushdir "${dir}"
        git pull > /dev/null 2>&1
        local cmdout=$?
        popdir
        return $cmdout
}
