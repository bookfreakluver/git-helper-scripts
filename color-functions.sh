#!/usr/bin/env bash

################################################################################
# This script contains terminal coloring/formatting codes and functions.
################################################################################

# 8/16 foreground color codes.
readonly FG_DEFAULT="39"
readonly FG_BLACK="30"
readonly FG_WHITE="97"
readonly FG_RED="31"
readonly FG_GREEN="32"
readonly FG_YELLOW="33"
readonly FG_BLUE="34"
readonly FG_MAGENTA="35"
readonly FG_CYAN="36"
readonly FG_LIGHT_GREY="37"

# 8/16 background color codes.
readonly BG_DEFAULT="49"
readonly BG_BLACK="30"
readonly BG_WHITE="97"
readonly BG_RED="31"
readonly BG_GREEN="32"
readonly BG_YELLOW="33"
readonly BG_BLUE="34"
readonly BG_MAGENTA="35"
readonly BG_CYAN="36"
readonly BG_LIGHT_GREY="37"

# Formatting options.
readonly FORMAT_BOLD="1"
readonly FORMAT_UNSET_BOLD="21"

################################################################################
# Reset text to all default settings.
# Usage: reset_text_defaults
################################################################################
reset_text_defaults() {
    tput sgr0
}

################################################################################
# Print the specified text in the color indicated by the specified color code.
# Text settings will be reset to default afterwards.
# Usage: colorize <color_code> <text>
################################################################################
colorize {
    echo -n -e "\e[${1}m${@:2}"
    reset_text_defaults
}

################################################################################
# Print the specified text in the color indicated by the specified color code,
# followed by a new line. Text settings will be reset to default afterwards.
# Usage: colorizeln <color_code> <text>
################################################################################
colorizeln {
    echo -e "\e[${1}m${@:2}"
    reset_text_defaults
}