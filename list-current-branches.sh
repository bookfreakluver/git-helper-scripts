#!/usr/bin/env bash

################################################################################
# Find all git worktrees either in the current directory or in a specified 
# directory and list the branch they're currently on.
################################################################################

set -o nounset
set -o errexit
set -o pipefail

__dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
__file="${__dir}/$( basename "${BASH_SOURCE[0]}")"
__base="$( basename "${__file}" .sh )"

source "${__dir}/common-functions.sh"

################################################################################
# Print usage instructions for this script.
# Globals:
#   __file
# Arguments:
#   None
# Usage:
#   usage
################################################################################
usage() {
        echo "Usage ${__file} [-d DIRECTORY] [options]"
        echo "  -d, --directory <directory> Directory to search for git repos, defaults to current dir"
        echo "  -s, --short-paths           Print base dir names instead of the fully-qualified paths"
        echo "  -h, --help                  Print this usage guide"
}

################################################################################
# Parse arguments and run the script.
# Globals:
#    None
# Arguments:
#    -d, --directory <directory> (optional) - defaults to current directory
#    -s, --short-paths (optional) - defaults to false
#    -h, --help (optional) - defaults to false
# Usage:
#   main [-d DIRECTORY] [-s] [-h]
################################################################################
main() {
        # Transform long options to short.
        for arg in "$@"; do
                shift
                case "${arg}" in
                        "--directory") set -- "$@" "-d" ;;
                        "--short-paths") set -- "$@" "-s" ;;
                        "--help") set -- "$@" "-h" ;;
                        *) set -- "$@" "${arg}" ;;
                esac
        done
        
        # Default behavior.
        local repo_dir
        repo_dir="$( pwd )"
        local print_short_paths=1
        
        # Parse short options
        while getopts "d:sh" opt; do
                case "${opt}" in
                        "d") repo_dir="${OPTARG}" ;;
                        "s") print_short_paths=0 ;;
                        "h") usage; exit 0 ;;
                        *) usage; exit 1 ;;
                esac
        done
        
        # Validate the directory.
        if [[ ! -d $repo_dir ]]; then
                echo "No such directory ${repo_dir}"
                exit 1
        fi
        
        # Find the worktrees and list their current branch.
        local git_dirs
        git_dirs="$( find_all_git_directories "${repo_dir}" )"
        for dir in $git_dirs; do
                local worktree
                worktree="$( dirname "${dir}" )"
                local current_branch
                current_branch="$( get_current_branch "${worktree}" )"
                if [[ $print_short_paths -eq 0 ]]; then
                        worktree="$( basename "${worktree}" )"
                fi
                # Print the branch name in yellow.
                echo -e "${worktree} \e[33m[${current_branch}]"
                tput sgr0 # Reset text color to default.
        done
}

main "$@"
